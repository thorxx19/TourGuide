package tourGuide;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import tourGuide.service.GpsUtilExecutorService;
import tourGuide.service.RewardExecutorService;
import tourGuide.service.RewardsService;

/**
 * @author froidefond
 */
@Configuration
public class TourGuideModule {

    @Bean
    public GpsUtilExecutorService getGpsUtil() {
        return new GpsUtilExecutorService();
    }

    @Bean
    public RewardsService getRewardsService() {
        return new RewardsService(getGpsUtil(), getRewardCentral());
    }

    @Bean
    public RewardExecutorService getRewardCentral() {
        return new RewardExecutorService();
    }

}
