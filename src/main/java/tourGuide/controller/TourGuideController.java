package tourGuide.controller;

import gpsUtil.location.Location;
import gpsUtil.location.VisitedLocation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import tourGuide.model.User;
import tourGuide.model.UserDto;
import tourGuide.model.UserReward;
import tourGuide.service.TourGuideService;
import tripPricer.Provider;

import java.util.List;
import java.util.Map;
import java.util.UUID;

/**
 * @author froidefond
 */
@RestController
public class TourGuideController {

    @Autowired
    TourGuideService tourGuideService;

    /**
     * endPoint pour verifier si l'api et active
     *
     * @return un string
     */
    @RequestMapping("/")
    public String index() {
        return "Greetings from TourGuide!";
    }

    /**
     * endPoint pour recup une location pour un user
     *
     * @param userName le name d'un user
     * @return location
     */
    @RequestMapping("/getLocation")
    public Location getLocation(@RequestParam String userName) {
        VisitedLocation visitedLocation = tourGuideService.getUserLocation(getUser(userName));
        return visitedLocation.location;
    }

    /**
     * endPoint qui renvoie les 5 attraction pour 1 utilisateur donner
     *
     * @param userName un nom d'user
     * @return un userDto
     */
    @RequestMapping("/getNearbyAttractions")
    public UserDto getNearbyAttractionsTest(@RequestParam String userName) {
        return tourGuideService.getListFiveAttraction(getUser(userName));
    }

    /**
     * endPoint qui renvoie une list d'userReward pour un user
     *
     * @param userName le nom d'un user
     * @return une liste UserReward
     */
    @RequestMapping("/getRewards")
    public List<UserReward> getRewards(@RequestParam String userName) {
        return tourGuideService.getUserRewards(getUser(userName));
    }

    /**
     * endPoint qui renvoie les position actuelle pour chaque utilisateur
     *
     * @return une Map UUID, Location
     */
    @RequestMapping("/getAllCurrentLocations")
    public Map<UUID, Location> getAllCurrentLocations() {
        return tourGuideService.userLastLocation();
    }

    /**
     * method qui renvoie une liste de provider
     *
     * @param userName le nom d'un user
     * @return une List Provider
     */
    @RequestMapping("/getTripDeals")
    public List<Provider> getTripDeals(@RequestParam String userName) {
        return tourGuideService.getTripDeals(getUser(userName));
    }

    /**
     * endPoint qui renvoie un user
     *
     * @param userName le nom d'un user
     * @return un user
     */
    @RequestMapping("/getUser")
    private User getUser(@RequestParam String userName) {
        return tourGuideService.getUser(userName);
    }

    /**
     * methos qui renvoie une liste d'user generer
     *
     * @param number le nombre d'user a generer
     * @return une List User
     */
    @RequestMapping("/userCount")
    public List<User> userCount(@RequestParam int number) {
        return tourGuideService.userCount(number);
    }


}