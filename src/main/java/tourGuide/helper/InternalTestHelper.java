package tourGuide.helper;

/**
 * @author froidefond
 */
public class InternalTestHelper {

    // Set this default up to 100,000 for testing
    private static int internalUserNumber = 100;

    /**
     * method pour recup le nombre d'utilisateur generer
     *
     * @return un nombre d'utilisateur
     */
    public static int getInternalUserNumber() {
        return internalUserNumber;
    }

    /**
     * method pour defenir le nombre d'utilisateur a generer
     *
     * @param internalUserNumber un nombre
     */
    public static void setInternalUserNumber(int internalUserNumber) {
        InternalTestHelper.internalUserNumber = internalUserNumber;
    }
}
