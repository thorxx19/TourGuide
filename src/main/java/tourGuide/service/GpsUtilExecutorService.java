package tourGuide.service;

import gpsUtil.GpsUtil;
import gpsUtil.location.Attraction;
import gpsUtil.location.VisitedLocation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import tourGuide.model.User;

import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * @author froidefond
 */
public class GpsUtilExecutorService {

    private final GpsUtil gpsUtil;
    private final ExecutorService executor = Executors.newFixedThreadPool(240);
    private final Logger logger = LoggerFactory.getLogger(GpsUtilExecutorService.class);

    /**
     * construct
     */
    public GpsUtilExecutorService() {
        this.gpsUtil = new GpsUtil();
    }

    /**
     * method qui renvoie une liste d'attraction
     *
     * @return une liste d'attractiion
     */
    public List<Attraction> getAttractions() {

        return gpsUtil.getAttractions();
    }

    /**
     * method pour gerer un object de type VisitLocation avec CompletableFuture
     *
     * @param user un utilisateur
     * @return un completableFuture
     */
    public CompletableFuture<VisitedLocation> getUserLocation(User user) {

        return CompletableFuture.supplyAsync(() -> gpsUtil.getUserLocation(user.getUserId()), executor).exceptionally(ex -> {
            logger.error("Error: ", ex);
            return null;
        });
    }

    /**
     * methode pour arreter l'executor
     */
    public void shutdown() {
        executor.shutdown();
    }

    /**
     * methode pour arrter l'executor en auto
     *
     * @throws Throwable capter une erreur
     */
    @Override
    protected void finalize() throws Throwable {
        try {
            shutdown();
        } finally {
            super.finalize();
        }
    }

}
