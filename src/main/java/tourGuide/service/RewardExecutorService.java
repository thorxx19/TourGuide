package tourGuide.service;

import gpsUtil.location.Attraction;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import rewardCentral.RewardCentral;
import tourGuide.model.User;

import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * @author froidefond
 */
public class RewardExecutorService {

    private final RewardCentral rewardCentral = new RewardCentral();

    private final ExecutorService executor = Executors.newFixedThreadPool(240);

    private final Logger logger = LoggerFactory.getLogger(RewardExecutorService.class);

    /**
     * method pour generer un reward point
     *
     * @param attraction l'attraction
     * @param user un utilisateur
     * @return un int
     */
    public CompletableFuture<Integer> getAttractionRewardPoints(Attraction attraction, User user) {

        return CompletableFuture.supplyAsync(() -> rewardCentral.getAttractionRewardPoints(attraction.attractionId, user.getUserId()), executor).exceptionally(ex -> {
            logger.error("Error: ", ex);
            return null;
        });
    }

    /**
     * methode pour stopper l'executor
     */
    public void shutdown() {
        executor.shutdown();
    }

    /**
     * methode pour stopper l'executor
     *
     * @throws Throwable capter une erreur
     */
    @Override
    protected void finalize() throws Throwable {
        try {
            shutdown();
        } finally {
            super.finalize();
        }
    }


}
