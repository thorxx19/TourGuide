package tourGuide.service;

import gpsUtil.GpsUtil;
import gpsUtil.location.Attraction;
import gpsUtil.location.Location;
import gpsUtil.location.VisitedLocation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import tourGuide.helper.InternalTestHelper;
import tourGuide.model.AttractionDto;
import tourGuide.model.User;
import tourGuide.model.UserDto;
import tourGuide.model.UserReward;
import tourGuide.tracker.Tracker;
import tripPricer.Provider;
import tripPricer.TripPricer;

import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.util.*;
import java.util.concurrent.CompletableFuture;
import java.util.stream.IntStream;

/**
 * @author froidefond
 */
@Service
public class TourGuideService {
    /**********************************************************************************
     *
     * Methods Below: For Internal Testing
     *
     **********************************************************************************/
    private static final String tripPricerApiKey = "test-server-api-key";
    public final Tracker tracker;
    private final RewardsService rewardsService;
    private final GpsUtilExecutorService gpsUtil;
    private final TripPricer tripPricer = new TripPricer();
    // Database connection will be used for external users, but for testing purposes internal users are provided and stored in memory
    private final Map<String, User> internalUserMap = new HashMap<>();
    private final Logger logger = LoggerFactory.getLogger(TourGuideService.class);
    boolean testMode = true;

    /**
     * construct
     *
     * @param gpsUtil        class GpsUtilExecutorService
     * @param rewardsService class RewardsService
     */
    public TourGuideService(GpsUtilExecutorService gpsUtil, RewardsService rewardsService) {
        this.gpsUtil = gpsUtil;
        this.rewardsService = rewardsService;

        if (testMode) {
            logger.info("TestMode enabled");
            logger.debug("Initializing users");
            initializeInternalUsers();
            logger.debug("Finished initializing users");
        }
        tracker = new Tracker(this);
        addShutDownHook();
    }

    /**
     * method pour gener x number de user
     *
     * @param number un nombre x d'utilisateur
     * @return une list d'utilisateur
     */
    public List<User> userCount(int number) {
        return initUserCount(number);
    }

    /**
     * method qui renvoie les 5 attraction pour 1 utilisateur donner
     *
     * @param user un utilisateur
     * @return un object userDto
     */
    public UserDto getListFiveAttraction(User user) {

        VisitedLocation visitedLocation = user.getLastVisitedLocation();
        UserDto userDto = new UserDto();
        List<AttractionDto> nearbyAttractions = new ArrayList<>();
        rewardsService.setAttractionProximityRange(user.getUserPreferences().getAttractionProximity());
        rewardsService.setProximityBuffer(6000);
        for (Attraction attraction : gpsUtil.getAttractions()) {
            AttractionDto attractionDto = new AttractionDto();
            double distanceMiles = rewardsService.getDistance(attraction, visitedLocation.location);
            if (rewardsService.isWithinAttractionProximity(attraction, visitedLocation.location)) {
                attractionDto.setAttractionName(attraction.attractionName);
                attractionDto.setLatitude(attraction.latitude);
                attractionDto.setLongitude(attraction.longitude);
                attractionDto.setInterval(distanceMiles);
                for (UserReward userReward : user.getUserRewards()) {
                    if (userReward.getAttraction().attractionName.equals(attraction.attractionName)) {
                        attractionDto.setRewardPoints(userReward.getRewardPoints());
                    }
                }
                nearbyAttractions.add(attractionDto);
                if (nearbyAttractions.size() == 5) {
                    break;
                }
            }
        }
        userDto.setUserName(user.getUserName());
        userDto.setLatitude(visitedLocation.location.latitude);
        userDto.setLongitude(visitedLocation.location.longitude);
        userDto.setAttraction(nearbyAttractions);
        return userDto;
    }

    /**
     * method qui recupére une list userReward
     *
     * @param user un utilisateur
     * @return une list d'userReward
     */
    public List<UserReward> getUserRewards(User user) {
        rewardsService.setAttractionProximityRange(user.getUserPreferences().getAttractionProximity());
        rewardsService.setProximityBuffer(6000);
        rewardsService.calculateRewards(user).join();
        return user.getUserRewards();
    }

    /**
     * method qui retourne une visitLocation pour un utilisateur
     *
     * @param user un utilisateur
     * @return un visitLocation
     */
    public VisitedLocation getUserLocation(User user) {
        return (!user.getVisitedLocations().isEmpty()) ?
                user.getLastVisitedLocation() :
                trackUserLocation(user).join();
    }

    /**
     * method qui renvoie la derniere location pour une liste d'utilisateur
     *
     * @return un object avec l'id et la location des utilisateur
     */
    public Map<UUID, Location> userLastLocation() {
        List<User> users = getAllUsers();
        Map<UUID, Location> currentLocation = new HashMap<>();
        users.parallelStream().forEach(user -> currentLocation.put(user.getUserId(), this.getUserLocation(user).location));
        return currentLocation;
    }

    /**
     * method qui renvoie un utilisateur avec le nom en parametre
     *
     * @param userName le nom de utilisateur
     * @return un user
     */
    public User getUser(String userName) {
        return internalUserMap.get(userName);
    }

    /**
     * method qui renvoie une liste d'utilisateur
     *
     * @return une List User
     */
    public List<User> getAllUsers() {
        return new ArrayList<>(internalUserMap.values());
    }

    /**
     * method pour ajouter une liste un utilisateur
     *
     * @param user un utilisateur
     */
    public void addUser(User user) {
        if (!internalUserMap.containsKey(user.getUserName())) {
            internalUserMap.put(user.getUserName(), user);
        }
    }

    /**
     * method qui retourne une liste de provider
     *
     * @param user un utilisateur
     * @return une List Provider
     */
    public List<Provider> getTripDeals(User user) {
        int cumulatativeRewardPoints = user.getUserRewards().stream().mapToInt(UserReward::getRewardPoints).sum();
        List<Provider> providers = tripPricer.getPrice(tripPricerApiKey, user.getUserId(), user.getUserPreferences().getNumberOfAdults(),
                user.getUserPreferences().getNumberOfChildren(), user.getUserPreferences().getTripDuration(), cumulatativeRewardPoints);
        user.setTripDeals(providers);
        return providers;
    }

    /**
     * method qui gere l'ajout d'une visitLocation et un userReward par utilisateur
     *
     * @param user un utilisateur
     * @return CompletableFuture VisitedLocation
     */
    public CompletableFuture<VisitedLocation> trackUserLocation(User user) {
        return gpsUtil.getUserLocation(user).thenApply(visitedLocation -> {
            user.addToVisitedLocations(visitedLocation);
            rewardsService.calculateRewards(user).join();
            return visitedLocation;
        });
    }

    /**
     * method qui return une list d'attraction par rapport a une visitLocation
     *
     * @param visitedLocation une visitLocation
     * @return une List Attraction
     */
    public List<Attraction> getNearByAttractions(VisitedLocation visitedLocation) {
        List<Attraction> nearbyAttractions = new ArrayList<>();
        for (Attraction attraction : gpsUtil.getAttractions()) {
            if (rewardsService.isWithinAttractionProximity(attraction, visitedLocation.location)) {
                nearbyAttractions.add(attraction);
            }
        }

        return nearbyAttractions;
    }

    /**
     * method pour stopper le tracking
     */
    private void addShutDownHook() {
        Runtime.getRuntime().addShutdownHook(new Thread(tracker::stopTracking));
    }

    /**
     * method qui génere X utilisateur pour les test
     */
    private void initializeInternalUsers() {

        GpsUtil gpsUtil1 = new GpsUtil();
        Attraction attraction = gpsUtil1.getAttractions().get(0);

        IntStream.range(0, InternalTestHelper.getInternalUserNumber()).forEach(i -> {
            String userName = "User" + i;
            String phone = "000";
            String email = userName + "@tourGuide.com";
            User user = new User(UUID.randomUUID(), userName, phone, email);
            generateUserLocationHistory(user);

            internalUserMap.put(userName, user);
        });
        logger.debug("Created " + InternalTestHelper.getInternalUserNumber() + " internal test users.");
    }

    /**
     * method qui génére X utilisateur
     *
     * @param number le nombre d'utilisateur a generer
     * @return une List<User>
     */
    private List<User> initUserCount(int number) {

        List<User> users = new ArrayList<>();
        Attraction attraction = gpsUtil.getAttractions().get(0);
        IntStream.range(0, number).forEach(i -> {
            String userName = "User" + i;
            String phone = "000";
            String email = userName + "@tourGuide.com";
            User user = new User(UUID.randomUUID(), userName, phone, email);
            generateUserLocationHistory(user);
            users.add(user);

        });
        logger.debug("Created " + number + " internal test users.");
        return users;
    }

    /**
     * method qui genere une visitLocation
     *
     * @param user un utilisateur
     */
    private void generateUserLocationHistory(User user) {
        IntStream.range(0, 3).forEach(i -> {
            user.addToVisitedLocations(new VisitedLocation(user.getUserId(), new Location(generateRandomLatitude(), generateRandomLongitude()), getRandomTime()));
        });
    }

    /**
     * method qui genere un random longitude
     *
     * @return un double
     */
    private double generateRandomLongitude() {
        double leftLimit = -180;
        double rightLimit = 180;
        return leftLimit + new Random().nextDouble() * (rightLimit - leftLimit);
    }

    /**
     * method qui genere une latitude
     *
     * @return un double
     */
    private double generateRandomLatitude() {
        double leftLimit = -85.05112878;
        double rightLimit = 85.05112878;
        return leftLimit + new Random().nextDouble() * (rightLimit - leftLimit);
    }

    /**
     * method qui genere un random time
     *
     * @return une Date
     */
    private Date getRandomTime() {
        LocalDateTime localDateTime = LocalDateTime.now().minusDays(new Random().nextInt(30));
        return Date.from(localDateTime.toInstant(ZoneOffset.UTC));
    }

}
