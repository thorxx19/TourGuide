package tourGuide.service;


import gpsUtil.location.Attraction;
import gpsUtil.location.Location;
import gpsUtil.location.VisitedLocation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import tourGuide.model.User;
import tourGuide.model.UserReward;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CompletableFuture;

/**
 * @author froidefond
 */
@Service
public class RewardsService {

    private static final double STATUTE_MILES_PER_NAUTICAL_MILE = 1.15077945;
    private static final int EARTH_RADIUS_NM = 3440;
    private final Logger logger = LoggerFactory.getLogger(RewardsService.class);
    // proximity in miles
    private final int defaultProximityBuffer = 10;
    private final int attractionProximityRange = 200;
    private final GpsUtilExecutorService gpsUtil;
    private final RewardExecutorService rewardsCentral;
    private int proximityBuffer = defaultProximityBuffer;
    private int proximityRange = attractionProximityRange;


    /**
     * construc
     *
     * @param gpsUtil       la classe GpsUtilExecutorService
     * @param rewardCentral la classe RewardExecutorService
     */
    public RewardsService(GpsUtilExecutorService gpsUtil, RewardExecutorService rewardCentral) {
        this.gpsUtil = gpsUtil;
        this.rewardsCentral = rewardCentral;
    }

    /**
     * method pour modifier le proximityBuffer
     *
     * @param proximityBuffer un entier
     */
    public void setProximityBuffer(int proximityBuffer) {
        this.proximityBuffer = proximityBuffer;
    }

    /**
     * method pour modifier le poximityRange
     *
     * @param proximityRange un entier
     */
    public void setAttractionProximityRange(int proximityRange) {
        this.proximityRange = proximityRange;
    }

    /**
     * method pour ajouter a un utilisateur un userReward
     *
     * @param user un utilisateur
     * @return un CompletableFuture
     */
    public CompletableFuture<Void> calculateRewards(User user) {

        RewardExecutorService rewardCentralService = new RewardExecutorService();

        List<VisitedLocation> userLocations = new ArrayList<>(user.getVisitedLocations());
        List<Attraction> attractions = gpsUtil.getAttractions();


        Map<Attraction, Integer> rewardPoints = new HashMap<>();
        Map<Attraction, VisitedLocation> rewardVisitedLocations = new HashMap<>();
        Map<Attraction, CompletableFuture<Void>> rewardFutures = new HashMap<>();


        userLocations.forEach(visitedLocation -> {
            attractions.forEach(attraction -> {
                if (user.getUserRewards().stream().noneMatch(r -> r.attraction.attractionName.equals(attraction.attractionName)) && nearAttraction(visitedLocation, attraction)) {
                    rewardFutures.putIfAbsent(attraction, rewardCentralService.getAttractionRewardPoints(attraction, user).thenAccept(rewardPoint -> rewardPoints.put(attraction, rewardPoint)));
                    rewardVisitedLocations.putIfAbsent(attraction, visitedLocation);
                }
            });
        });


        return CompletableFuture.allOf(rewardFutures.values().toArray(new CompletableFuture[0]))
                .thenAccept(unused -> rewardPoints.forEach((attraction, rewardPoint) ->
                        user.addUserReward(new UserReward(rewardVisitedLocations.get(attraction), attraction, rewardPoint))
                ));
    }

    /**
     * method qui compare une distance au proximityRange
     *
     * @param attraction une attraction
     * @param location   une localisation
     * @return un boolean
     */
    public boolean isWithinAttractionProximity(Attraction attraction, Location location) {
        return (getDistance(attraction, location) < proximityRange);
    }

    /**
     * method qui compare une distance au proximityRange
     *
     * @param visitedLocation une localisation de visit
     * @param attraction      une attraction
     * @return un boolean
     */
    private boolean nearAttraction(VisitedLocation visitedLocation, Attraction attraction) {
        return (getDistance(attraction, visitedLocation.location) < proximityBuffer);
    }


    /**
     * method qui calcul la distance entre 2 point
     *
     * @param loc1 point N°1
     * @param loc2 point N°2
     * @return un double
     */
    public double getDistance(Location loc1, Location loc2) {
        double lat1 = Math.toRadians(loc1.latitude);
        double lon1 = Math.toRadians(loc1.longitude);
        double lat2 = Math.toRadians(loc2.latitude);
        double lon2 = Math.toRadians(loc2.longitude);

        double angle = Math.acos(Math.sin(lat1) * Math.sin(lat2)
                + Math.cos(lat1) * Math.cos(lat2) * Math.cos(lon1 - lon2));

        double nauticalMiles = 60 * Math.toDegrees(angle);
        return STATUTE_MILES_PER_NAUTICAL_MILE * nauticalMiles;
    }
}
