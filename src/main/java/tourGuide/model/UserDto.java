package tourGuide.model;

import java.util.List;

/**
 * @author froidefond
 */
public class UserDto {

    private String userName;
    private double longitude;
    private double latitude;
    private List<AttractionDto> attraction;


    //getter et setter


    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public List<AttractionDto> getAttraction() {
        return attraction;
    }

    public void setAttraction(List<AttractionDto> attraction) {
        this.attraction = attraction;
    }

}
