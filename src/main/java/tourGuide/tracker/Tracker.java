package tourGuide.tracker;


/**
 * @author froidefond
 */

import org.apache.commons.lang3.time.StopWatch;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import tourGuide.model.User;
import tourGuide.service.TourGuideService;

import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;


public class Tracker extends Thread {
    private final ExecutorService executorService = Executors.newFixedThreadPool(250);
    private final TourGuideService tourGuideService;
    private final Logger logger = LoggerFactory.getLogger(Tracker.class);
    private boolean stop = false;
    private boolean testMode = true;

    /**
     * construct
     *
     * @param tourGuideService class TourGuideService
     */
    public Tracker(TourGuideService tourGuideService) {
        this.tourGuideService = tourGuideService;

        executorService.submit(this);
    }

    /**
     * Assures to shut down the Tracker thread
     */
    public void stopTracking() {
        stop = true;
        executorService.shutdownNow();
    }

    public void setTestMode(boolean testMode) {
        this.testMode = testMode;
    }

    /**
     * method qui call la methode trackUserLocation
     */
    @Override
    public void run() {
        StopWatch stopWatch = new StopWatch();

        List<User> users = tourGuideService.getAllUsers();
        logger.debug("Begin Tracker. Tracking " + users.size() + " users.");
        stopWatch.start();

        users.forEach(user -> {
            if (testMode) {
                tourGuideService.trackUserLocation(user).join();
            }
        });
        stopWatch.stop();
        logger.debug("Tracker Time Elapsed: " + TimeUnit.MILLISECONDS.toMillis(stopWatch.getTime()) + " milliseconds.");
        stopWatch.reset();

    }
}
