package tourGuide;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.util.Locale;

/**
 * @author froidefond
 */
@SpringBootApplication
public class Application {
    /**
     * method principal
     *
     * @param args un argument
     */
    public static void main(String[] args) {
        Locale.setDefault(Locale.UK);
        SpringApplication.run(Application.class, args);
    }

}
