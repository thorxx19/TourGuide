package tourGuide;

import gpsUtil.location.Attraction;
import gpsUtil.location.Location;
import gpsUtil.location.VisitedLocation;
import org.apache.commons.lang3.time.StopWatch;
import org.junit.Test;
import tourGuide.helper.InternalTestHelper;
import tourGuide.model.User;
import tourGuide.service.GpsUtilExecutorService;
import tourGuide.service.RewardExecutorService;
import tourGuide.service.RewardsService;
import tourGuide.service.TourGuideService;
import tripPricer.Provider;

import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.UUID;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

/**
 * @author froidefond
 */
public class TestTourGuideService {

    @Test
    public void getUserLocation() {
        Locale.setDefault(Locale.UK);
        GpsUtilExecutorService gpsUtil = new GpsUtilExecutorService();
        RewardsService rewardsService = new RewardsService(gpsUtil, new RewardExecutorService());
        InternalTestHelper.setInternalUserNumber(0);
        TourGuideService tourGuideService = new TourGuideService(gpsUtil, rewardsService);

        User user = new User(UUID.randomUUID(), "jon", "000", "jon@tourGuide.com");
        VisitedLocation visitedLocation = tourGuideService.trackUserLocation(user).join();
        tourGuideService.tracker.stopTracking();
        assertEquals(visitedLocation.userId, user.getUserId());
    }

    @Test
    public void addUser() {
        Locale.setDefault(Locale.UK);
        GpsUtilExecutorService gpsUtil = new GpsUtilExecutorService();
        RewardsService rewardsService = new RewardsService(gpsUtil, new RewardExecutorService());
        InternalTestHelper.setInternalUserNumber(0);
        TourGuideService tourGuideService = new TourGuideService(gpsUtil, rewardsService);

        User user = new User(UUID.randomUUID(), "jon", "000", "jon@tourGuide.com");
        User user2 = new User(UUID.randomUUID(), "jon2", "000", "jon2@tourGuide.com");

        tourGuideService.addUser(user);
        tourGuideService.addUser(user2);

        User retrivedUser = tourGuideService.getUser(user.getUserName());
        User retrivedUser2 = tourGuideService.getUser(user2.getUserName());

        tourGuideService.tracker.stopTracking();

        assertEquals(user, retrivedUser);
        assertEquals(user2, retrivedUser2);
    }

    @Test
    public void getAllUsers() {
        Locale.setDefault(Locale.UK);
        GpsUtilExecutorService gpsUtil = new GpsUtilExecutorService();
        RewardsService rewardsService = new RewardsService(gpsUtil, new RewardExecutorService());
        InternalTestHelper.setInternalUserNumber(0);
        TourGuideService tourGuideService = new TourGuideService(gpsUtil, rewardsService);

        User user = new User(UUID.randomUUID(), "jon", "000", "jon@tourGuide.com");
        User user2 = new User(UUID.randomUUID(), "jon2", "000", "jon2@tourGuide.com");

        tourGuideService.addUser(user);
        tourGuideService.addUser(user2);

        List<User> allUsers = tourGuideService.getAllUsers();

        tourGuideService.tracker.stopTracking();

        assertTrue(allUsers.contains(user));
        assertTrue(allUsers.contains(user2));
    }

    @Test
    public void trackUser() {
        Locale.setDefault(Locale.UK);
        GpsUtilExecutorService gpsUtil = new GpsUtilExecutorService();
        RewardsService rewardsService = new RewardsService(gpsUtil, new RewardExecutorService());
        InternalTestHelper.setInternalUserNumber(0);
        TourGuideService tourGuideService = new TourGuideService(gpsUtil, rewardsService);

        User user = new User(UUID.randomUUID(), "jon", "000", "jon@tourGuide.com");
        VisitedLocation visitedLocation = tourGuideService.trackUserLocation(user).join();

        tourGuideService.tracker.stopTracking();

        assertEquals(user.getUserId(), visitedLocation.userId);
    }

    // Not yet implemented
    @Test
    public void getNearbyAttractions() {
        Locale.setDefault(Locale.UK);
        GpsUtilExecutorService gpsUtil = new GpsUtilExecutorService();
        RewardsService rewardsService = new RewardsService(gpsUtil, new RewardExecutorService());
        InternalTestHelper.setInternalUserNumber(0);
        TourGuideService tourGuideService = new TourGuideService(gpsUtil, rewardsService);

        User user = new User(UUID.randomUUID(), "jon", "000", "jon@tourGuide.com");
        Location location = new Location(40.50571215406855, -74.05871215406855);
        Date date = new Date();

        VisitedLocation visitedLocation = new VisitedLocation(user.getUserId(), location, date);

        StopWatch stopWatch = new StopWatch();
        stopWatch.start();

        List<Attraction> attractions = tourGuideService.getNearByAttractions(visitedLocation);

        tourGuideService.tracker.stopTracking();

        assertEquals(4, attractions.size());
    }

    @Test
    public void getTripDeals() {
        Locale.setDefault(Locale.UK);
        GpsUtilExecutorService gpsUtil = new GpsUtilExecutorService();
        RewardsService rewardsService = new RewardsService(gpsUtil, new RewardExecutorService());
        InternalTestHelper.setInternalUserNumber(0);
        TourGuideService tourGuideService = new TourGuideService(gpsUtil, rewardsService);

        User user = new User(UUID.randomUUID(), "jon", "000", "jon@tourGuide.com");

        List<Provider> providers = tourGuideService.getTripDeals(user);

        tourGuideService.tracker.stopTracking();

        assertEquals(5, providers.size());
    }
}
