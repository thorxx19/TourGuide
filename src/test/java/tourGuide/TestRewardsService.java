package tourGuide;

import gpsUtil.location.Attraction;
import gpsUtil.location.VisitedLocation;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import tourGuide.helper.InternalTestHelper;
import tourGuide.model.User;
import tourGuide.model.UserReward;
import tourGuide.service.GpsUtilExecutorService;
import tourGuide.service.RewardExecutorService;
import tourGuide.service.RewardsService;
import tourGuide.service.TourGuideService;

import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.UUID;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

/**
 * @author froidefond
 */
public class TestRewardsService {
    private final Logger logger = LoggerFactory.getLogger(TestRewardsService.class);

    @Test
    public void userGetRewards() throws Exception {
        Locale.setDefault(Locale.UK);
        GpsUtilExecutorService gpsUtil = new GpsUtilExecutorService();
        RewardsService rewardsService = new RewardsService(gpsUtil, new RewardExecutorService());

        InternalTestHelper.setInternalUserNumber(0);
        TourGuideService tourGuideService = new TourGuideService(gpsUtil, rewardsService);

        User user = new User(UUID.randomUUID(), "jon", "000", "jon@tourGuide.com");
        Attraction attraction = gpsUtil.getAttractions().get(0);
        user.addToVisitedLocations(new VisitedLocation(user.getUserId(), attraction, new Date()));
        tourGuideService.trackUserLocation(user).join();
        List<UserReward> userRewards = user.getUserRewards();
        tourGuideService.tracker.stopTracking();
        assertEquals(1, userRewards.size());
    }

    @Test
    public void isWithinAttractionProximity() {
        Locale.setDefault(Locale.UK);
        GpsUtilExecutorService gpsUtil = new GpsUtilExecutorService();
        RewardsService rewardsService = new RewardsService(gpsUtil, new RewardExecutorService());
        Attraction attraction = gpsUtil.getAttractions().get(0);
        assertTrue(rewardsService.isWithinAttractionProximity(attraction, attraction));
    }

    // Needs fixed - can throw ConcurrentModificationException
    @Test
    public void nearAllAttractions() {
        GpsUtilExecutorService gpsUtilService = new GpsUtilExecutorService();
        RewardsService rewardsService = new RewardsService(gpsUtilService, new RewardExecutorService());
        rewardsService.setProximityBuffer(Integer.MAX_VALUE);

        InternalTestHelper.setInternalUserNumber(1);
        TourGuideService tourGuideService = new TourGuideService(gpsUtilService, rewardsService);
        List<User> users = tourGuideService.getAllUsers();
        tourGuideService.tracker.setTestMode(false);

        users.forEach(user -> {
            rewardsService.calculateRewards(user).join();
        });

        List<UserReward> userRewards = tourGuideService.getUserRewards(tourGuideService.getAllUsers().get(0));

        tourGuideService.tracker.stopTracking();

        assertEquals(gpsUtilService.getAttractions().size(), userRewards.size());
    }

}
