<img align="left" width="250" src="https://user.oc-static.com/upload/2019/10/22/15717497085899_15717361053964_image1.jpg">

# TourGuide 

TourGuide est un projet 'Gradle' dont le but est localiser des attractions en fonction d'un point GPS.

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. See deployment for notes on how to deploy the project on a live system.

### Prerequisites

What things you need to install the software and how to install them

- Java 17
- Gradle 8.1
- Jacoco 0.8.10

### Installing

A step by step series of examples that tell you how to get a development env running:

1.Install Java:

https://docs.oracle.com/javase/8/docs/technotes/guides/install/install_overview.html

2.Install Gradle:

https://gradle.org/install/




## GitLab Back SpingBoot
Link [TourGuide](https://gitlab.com/thorxx19/TourGuide)

## Installation TourGuide

Utiliser [gradle](https://gradle.org/) pour compiler le projet

```bash
./gradlew build
```

## Usage

```bash
java -jar build/libs/TourGuide-1.0.0.jar
```
